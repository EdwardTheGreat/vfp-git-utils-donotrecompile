set message to										&& makes the message show at bottom of screen if has been hidden
close all
clear All
if type([_screen.cthordispatcher])=[U]
	if wexist([properties])
		hide wind [properties]					&& runs faster when this window is not constantly updated
		endif
	do thor.app
	endif
priva git,gitlog
gitlog=[push]
gitout([Running GitPush.prg  v1.00: ]+ttoc(datetime()))
set proc to c:\carpet\thor\tools\procs\thor_proc_gitutilities.prg addi
git		=thor_proc_gitutilities_norecompile()   && sublclassed just so I can prevent recompiling of all programs after pulling
*git		=thor_proc_gitutilities_eddie()	
oErr		=.null.
do binmenu with [c:\carpet\monthly.mnx]
do binmenu with [c:\carpet\padother.mnx]
do binmenu with [c:\carpet\payroll.mnx]
do binmenu with [c:\carpet\pricelst.mnx]
do binmenu with [c:\carpet\quickbooks.mnx]
do binmenu with [c:\carpet\recv.mnx]
do binmenu with [c:\carpet\sampledept.mnx]
do binmenu with [c:\carpet\shiprecv.mnx]
do binmenu with [c:\carpet\status.mnx]
do binmenu with [c:\carpet\undo.mnx]
do binmenu with [c:\carpet\webstore.mnx]
do binmenu with [c:\carpet\cust.mnx]
do binmenu with [c:\carpet\eddie.mnx]
do binmenu with [c:\carpet\invent.mnx]
do binmenu with [c:\carpet\main.mnx]
do binmenu with [c:\carpet\menu.mnx]
do binmenu with [c:\carpet\advertis.mnx]
do binproj with [c:\carpet\carpet.pjx]
do binproj with [c:\wconnect\carpetwholesale.pjx]
do binproj with [c:\wconnect\discountfloor.pjx]
do binproj with [c:\wconnect\house.pjx]
do binproj with [c:\wconnect\testhash.pjx]

do gitpush1 with [c:\carpet]
do gitpush1 with [c:\wconnect]
do gitpush1 with [c:\inetpub\wwwroot\carpet-wholesale]
acti wind command
rele git, gitlog
retu

proce binproj
lpara xProj
lProj=git.BinaryToTextProjectFile(@oErr,xProj)
gitout(xProj+[ to Binary: ]+iif(lProj,[Success],[*** FAILURE ***]))
if lProj=.f.
	gitout(oErr.details)
	gitout(oErr.message)
	endif
retu

proce binMenu
lpara xMenu
lMenu=git.BinaryToTextProjectFile(@oErr,xMenu)
gitout(xMenu+[ to Binary: ]+iif(lMenu,[Success],[*** FAILURE ***]))
if lMenu=.f.
	gitout(oErr.details)
	gitout(oErr.message)
	endif
retu

proce gitpush1
para xPath
gitout([])
gitout(xPath)
cresult	=[]
git.executecommand(@oErr,xPath,[git status],@cResult)
if ![On branch master]$cResult
	gitout(strtran(cResult,chr(10),chr(10)+chr(13)))
	messagebox([Not on Branch Master])
	retu
	endif
set safe off
lBinary=git.BinaryToTextRepo(@oErr,xPath,.t.)		&& generate text files and undo changes because of recompile
set safe on
if lBinary=.t.
	gitout([Generated text files and undo changes because of recompile])
	else
	gitout([*** FAILED text files and undo changes because of recompile])
	if type([oErr.Details])<>[U]
		gitout(oErr.details)
		gitout(oErr.message)
		return
		endif
	endif
oResult	 = git.Getrepostatistics(@oErr,xPath)
*!*	\Staged   = <<oResult.stagedchanges>>
*!*	\UnStaged = <<oResult.unstagedchanges>>
*!*	\UnTracked= <<oResult.untrackeditems>>
*!*	\Conflicts= <<oResult.mergeconflicts>>
if oResult.untrackeditems>0
	if [inetpub]$xPath
		do gitadd with [*.BMP]
		do gitadd with [*.jpg]
		do gitadd with [*.JPG]
		do gitadd with [*.html]
		do gitadd with [*.htm]
		do gitadd with [*.css]
		else
		do gitadd with [*.prg]
		do gitadd with [*.PRG]
		do gitadd with [*.frx]
		do gitadd with [*.FRX]
		do gitadd with [*.frt]
		do gitadd with [*.FRT]
		do gitadd with [*.scx]
		do gitadd with [*.SCX]
		do gitadd with [*.sct]
		do gitadd with [*.SCT]
		do gitadd with [*.mnx]
		do gitadd with [*.mnt]
		endif
	gitout([git add complete])
	oResult	=git.Getrepostatistics(oErr,xPath)
	endif
gitout([Staged   = ]+trans(oResult.stagedchanges))
gitout([UnStaged = ]+trans(oResult.unstagedchanges))
gitout([UnTracked= ]+trans(oResult.untrackeditems))
gitout([Conflicts= ]+trans(oResult.mergeconflicts))
lStatus=git.executecommand(@oErr,xPath,[git status],@cResult)
*gitout(strtran(cResult,chr(10),chr(10)+chr(13)))
gitout(cResult)
if oResult.unstagedchanges=0
	gitout([No Files to Commit])
	else
	cMsg	 =[git commit -a -m ]+left(sys(0),9)
	gitout(cMsg)
	lCommit=git.executecommand(@oErr,xPath,cMsg,@cresult)
	gitout([Commit= ]+iif(lCommit,[Success],[*** FAILURE ***]))
	gitout(strtran(cResult,chr(10),chr(10)+chr(13)))
	if lCommit=.f.
		retur
		endif
	endif
if oResult.unstagedchanges+oResult.stagedchanges=0
	gitout([Nothing to Push])
	retur
	endif
lPush=git.executecommand(@oErr,xPath,[git push --all],@cresult)
gitout([Push=]+iif(lPush,[Success],[*** FAILURE ***]))
gitout(strtran(cResult,chr(10),chr(10)+chr(13)))
if isnull(lPush) or lPush=.f.
	gitout(oErr.details)
	gitout(oErr.message)
	return
	endif
return

proce gitadd
lpara xFileSpec
lCommit	=git.executecommand(@oErr,xPath,[git add ]+xFileSpec,@cresult)
if lCommit=.f.
	if [did not match any files]$oErr.message
		gitout(xFileSpec+[ not found.])
		else
		gitout([git add failed ]+xFileSpec)
		gitout([Result : ]+strtran(cResult,chr(10),chr(10)+chr(13)))
		gitout([Details: ]+oErr.details)
		gitout([Message: ]+oErr.message)
		endif
	endif
retu

*do gitbinary with [c:\carpet\carpet.pjx]
*!*	proce gitbinary
*!*	lpara xProj
*!*	*lBinary=git.BinaryToTextRepo(oErr,xPath,.t.)		&& generate text files and undo changes because of recompile
*!*	lBinary=git.BinaryToTextProject(oErr,xProj,.t.)		&& generate text files and undo changes because of recompile
*!*	if lBinary=.t.
*!*		?[Generated text files and undo changes because of recompile]
*!*		else
*!*		? oErr.details
*!*		? oErr.message
*!*		return
*!*		endif
*!*	retu


*!*	lChange= git.RepoHasChanges(oErr,xPath)		&& will be true if there is something you could add to the commit
*!*	if isnull(lChange)
*!*		?[Error detecting if Repo has Changes]
*!*		? oErr.details
*!*		? oErr.message
*!*		return
*!*		endif
*!*	if lChange=.f.
*!*		?? [   No Changes]
*!*		return
*!*		endif






*!*	set proc to c:\carpet\thor\tools\procs\thor_proc_gitutilities.prg
*!*	git		=Thor_Proc_GitUtilities()
*!*	*?git.selftest()
*!*	*?git.executecommand(oErr,[c:\carpet],[git status],cresult)

*!*	do gitpush1 with [c:\carpet]
*!*	do gitpush1 with [c:\wconnect]
*!*	do gitpush1 with [c:\inetpub\wwwroot\carpet-wholesale]
*!*	retu


*!*	proce gitpush1
*!*	lpara xPath
*!*	?
*!*	? xPath
*!*	cresult	=[]
*!*	oErr		=.null.
*!*	lBinary=git.BinaryToTextRepo(oErr,xPath,.t.)		&& generate text files and undo changes because of recompile
*!*	if lBinary=.t.
*!*		?[Generated text files and undo changes because of recompile]
*!*		else
*!*		? oErr.details
*!*		? oErr.message
*!*		return
*!*		endif
*!*	nFilestoUpload=gitstatus(xPath)
*!*	if nFilestoUpload=0
*!*		?[No Files to Commit]
*!*		return
*!*		endif
*!*	lChange= git.RepoHasChanges(oErr,xPath)
*!*	if isnull(lChange)
*!*		?[Error detecting if Repo has Changes]
*!*		? oErr.details
*!*		? oErr.message
*!*		return
*!*		endif
*!*	if lChange=.f.
*!*		?? [   No Changes]
*!*		return
*!*		endif
*!*	lCommit=git.executecommand(@oErr,xPath,[git commit -a -m FromHome],@cresult)
*!*	?[Commit= ],iif(lCommit,[Success],[*** FAILURE ***])
*!*	? cResult
*!*	if lCommit=.t.
*!*		lPush=git.executecommand(@oErr,xPath,[git push],@cresult)
*!*		?[Push=],iif(lPush,[Success],[*** FAILURE ***])
*!*		? cResult
*!*		if isnull(lPush) or lPush=.f.
*!*			? oErr.details
*!*			? oErr.message
*!*			return
*!*			endif
*!*		endif
*!*	return