* written by eddie caldwell 2016 - free to use copy and modify just don't copywrite or patent it!
* this is subclassing Mike Potjer's awesome and incredible and amazing program so it does not recompile the files each time
* I hate having all of the files recompiled because when I use windows explorer to look at the most recent changes,
* all I see is 1000 fxp files at the top of the list.  And the way I work, pretty much all of the compiling is handled in the project build
* the changes to the original are:
* 		Creates the Subclassed edGit(which is included below) instead of cusGitUtilities 
*		commented out #INCLUDE Thor_Proc_GitUtilities.h because it was causing error and is not needed here
*		commented out *	llSuccess = This.RecompileRepoProgramFiles( @m.toErrorInfo, m.tcRepository, @m.lcFileListAlias )
*		added 	gitout([No recompile PRG])				&& Displays and writes this text to log using gitout a function I wrote
*           this really is not needed other than to show me it is using my subclass and not recompiling.
LOCAL loGitUtils
set proc to c:\carpet\thor\tools\procs\thor_proc_gitutilities.prg

*loGitUtils = CREATEOBJECT( "cusGitUtilities" )
loGitUtils = CREATEOBJECT( "edGit" )

*-- If Thor is installed, return the object reference via the Thor
*-- dispatcher, making it easier to call this procedure via Thor,
*-- otherwise return the reference directly.
IF VARTYPE( _Screen.cThorDispatcher ) = "C" ;
		AND NOT EMPTY( _Screen.cThorDispatcher )
	RETURN EXECSCRIPT( _Screen.cThorDispatcher, "Result=", m.loGitUtils )
ELSE
	RETURN m.loGitUtils
ENDIF


*#INCLUDE Thor_Proc_GitUtilities.h

*********************************************************************
DEFINE CLASS edGit AS cusGitUtilities of thor_proc_gitutilities.prg

FUNCTION PostCheckoutProcessRepo

*!*	This method makes sure that files not committed to a repository are synchronized as needed to
*!*	the files in the repository, something that is frequently needed after a checkout.  This primarily
*!*	involves regenerating or recompiling certain files.

*!*	PARAMETERS:
*!*		  toErrorInfo (O) -	A variable passed by reference to store any error info returned from
*!*							this method.  This will be returned as an Exception object.  This
*!*							parameter is not required, but recommended.
*!*		 tcRepository (O) -	The full path to the repository folder to be processed.  If omitted,
*!*							the current folder will be used as the repository.

*!*	RETURNS: Logical
*!*		.T. if processing is successful, .F. if an error occurs.
*********************************************************************
LPARAMETERS toErrorInfo AS Exception, tcRepository AS String

LOCAL lcFileListAlias, ;
	llSuccess

*-- Make sure we have a unique name of the file list cursor, and that
*-- there are no prior instances open with with name.
lcFileListAlias = "C_GitRepoFileList" + SYS(2015)
USE IN SELECT( m.lcFileListAlias )

*-- Make sure all .PJX files are in synch with their corresponding
*-- text file.
llSuccess = This.RegenerateRepoProjects( @m.toErrorInfo, m.tcRepository, @m.lcFileListAlias )

*-- Make sure the program and object code files for all .MNX menu files
*-- are updated.
IF m.llSuccess
	llSuccess = This.RegenerateRepoMenus( @m.toErrorInfo, m.tcRepository, @m.lcFileListAlias )
ENDIF

*-- Make sure the object code for all program files is updated.
IF m.llSuccess
	gitout([No recompile PRG])				&& efc 07/26/2016 added. Displays and writes this text to log using gitout a function I wrote
	*	llSuccess = This.RecompileRepoProgramFiles( @m.toErrorInfo, m.tcRepository, @m.lcFileListAlias )		&& efc 07/26/2016 deleted  because I hate it recompiling all the files
ENDIF

*{ MJP -- 06/10/2015 01:40:07 PM - Begin
*-- Restore file timestamps for all repository files, if applicable.
IF m.llSuccess ;
		AND This.AreTimestampsPreserved( m.tcRepository )
	llSuccess = This.RestoreRepoTimestamps( @m.toErrorInfo, m.tcRepository )
ENDIF
*} MJP -- 06/10/2015 01:40:07 PM - End

USE IN SELECT( m.lcFileListAlias )

RETURN m.llSuccess
ENDFUNC


function ConvertVFPFile_public
LPARAMETERS toErrorInfo AS Exception, tcInputFilename AS String, tcRepository AS String, 	tlGenerateBinary AS Boolean

local cReturn
cReturn=this.ConvertVFPFile( toErrorInfo, tcInputFilename, tcRepository, tlGenerateBinary)
return cReturn

enddefine


