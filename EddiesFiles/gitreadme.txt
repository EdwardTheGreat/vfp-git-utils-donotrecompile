1-12-2017 added program fixproject.prg.  This program allows you to rebuild a project from the pj2 from the foxpro command window.  Maybe there is already a way to do this I am unfamiliar with.   Used when a project file gets corrupted.   I added a new method (convertvfpfile_public ) as a wrapper around the private method convertvfpfile.




thor_proc_gitutilities_norecompile.prg subclasses thor_proc_gitutilities so I can avoid recompiling all the prg files.

gitout.prg is a program that formats git output strings to be a little more friendly and saves them to a log.

gitpush.prg and gitpull.PRG are programs I wrote to automate the pushing and pulling from the command prompt using the amazing programs Mike Potjer created.

They might be helpful for someone trying to do the same thing for their projects.  You will need to customize the repository, project and menu names.


I have been using this for several months and it generally works pretty well as long as there are no conflicts to merge.
I push/pull from my computer at work, home and my laptop and another developer occasionally does so also so in how I use it, there are rarely conflicts.  I use tortoiseget to handle conflicts.

*******  Warning:  One issue is that occasionally after a pull, the PJX/PJT files get corrupted.   *****
When this happens, I delete the PJX/PJT/EXE/APP files and use foxbin2prg to recreate the project from the pj2 file, which works fine.

The strange thing about the error is that it occurs when I build the project from the command window:  [build Exe carpet from carpet]
But if I [modify project carpet] and then build the EXE using the Build button, it does not corrupt.
After that, I can build from the command line fine.


Occasionally when doing a push you will see, [FAILED text files and undo changes because of recompile].  Usually this is unimportant and you can do another push and it will clear it up.

On rare occasions, git hangs which also hangs foxpro.  If this happens you have to end the Git.exe process and then foxpro returns an error while trying to save the error log, so you can't see what the git error was.

One caveat for using Mike's gitutilities is when you first use it on a computer, there is a setting you need to set using git bash before using it from foxpro or else git hangs waiting on you to answer a dialog you can't see in foxpro. The setting has something to do with a behavior that was changed in git and you need to specify if you want the old behavior or new (something to do with fast forward I think).  So do a git commit/push where there is a conflict from the git bash ui and you will see what the error is and be able to figure out the issue.
 
** Use at your own risk.  No warranties on it working properly.  It could damage files!  